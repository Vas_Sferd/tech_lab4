#include <stdio.h>
#include <string.h>

#include "view.h"

static char buff[4096];

// Сериализация одного узла без потомков
void _btNodePrintS(btree_t * btree, size_t * position)
{
    char ** strs = btree->_strs;

    buff[(*position)++] = '\"';   // Кавычка

    // Печать
    for (size_t i = 0; i < btree->_cnt; ++i)
    {
        size_t offset = strlen(strs[i]);
        memcpy(&buff[*position], strs[i], offset * sizeof(char));
        *position = (*position + offset);
        buff[(*position)++] = ' ';
    }

    buff[*position - 1] = '\"';          // Кавычка
    buff[*position] = '\0';              // Завершение строки

    return;
}

void _btPrintS(btree_t * btree, size_t * position)
{
    if (btree && btree->_child)
    {
        for (btree_t * child = btree->_child; child != NULL; child = child->_right)
        {
            _btNodePrintS(btree, position);
            *position += sprintf(&buff[*position], " -> ");
            _btNodePrintS(child, position);
            *position += sprintf(&buff[*position], ";\n");
        }

        *position += sprintf(&buff[*position], "\n");

        for (btree_t * child = btree->_child; child != NULL; child = child->_right)
        {
            _btPrintS(child, position);
        }
    }
    else
    {
        _btNodePrintS(btree, position);
        *position += sprintf(&buff[*position], ";\n");
    }

    return;
}

char * btToStr(btree_t * btree)
{
    // Начальная позиция
    size_t position = 0;
    buff[position] = '\0';

    position += sprintf(&buff[position], "digraph tree\n{\n");

    // Сериализуем дерево
    _btPrintS(btree, &position);
    char * str = calloc(strlen(buff), sizeof(char));

    position += sprintf(&buff[position], "}");

    // Копируем результат
    strcpy(str, buff);

    return str;
}

void btView(btree_t * btree)
{
    puts(btToStr(btree));
}
