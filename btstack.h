#ifndef _BTSTACK_H
#define _BTSTACK_H

#include "types.h"

btstack_t * btstack_push(btstack_t * btstack, btree_t ** btree_ptr);
btstack_t * btstack_pop(btstack_t * btstack);

#endif
