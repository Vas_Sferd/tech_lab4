#ifndef _VIEW_H
#define _VIEW_H

#include "types.h"

char * btToStr(btree_t * btree);
void btView(btree_t * btree);

#endif
