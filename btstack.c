#include "btstack.h"

btstack_t * btstack_push(btstack_t * btstack, btree_t ** btree_ptr)
{
    btstack_t * st = (btstack_t *)malloc(sizeof(btstack_t));
    st->parent = btstack;
    st->btree_ptr = btree_ptr;
    return st;
}

btstack_t * btstack_pop(btstack_t * btstack)
{
    if (btstack)
    {
        btstack_t * st = btstack->parent;
        free(btstack);
        return st;
    }
    else
    {
        return NULL;
    }
}
