#ifndef _TYPES_H
#define _TYPES_H

#include <stdlib.h>
#include <stdbool.h>

typedef struct btree_t
{
    size_t _rate;       // Порядок дерева
    size_t _cnt;        // Число ключей в узле
    char ** _strs;      // Значение ключа
    struct btree_t * _child;   // Список потомков
    struct btree_t * _right;   // Пр

} btree_t;

typedef struct btstack_t
{
    struct btstack_t * parent;
    btree_t ** btree_ptr;

} btstack_t;

#endif
