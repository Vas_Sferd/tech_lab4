#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "btstack.h"
#include "view.h"
#include "types.h"

// Проверка корректности дерева
bool btIsValid(const btree_t * btree)
{
    bool result = true;

    if (btree)
    {
        size_t cnt = btree->_cnt;       // Число ключей
        size_t rate = btree->_rate;     // Степень дерева

        // Максимум, который может физически и теоретически поместиться в узел
        if (cnt < rate - 1)
        {
            // Проверка на упорядоченность элементов
            for (int i = 0; i < (int)cnt - 2; ++i)
            {
                if (strcmp(btree->_strs[i], btree->_strs[i + 1]) > 0)
                {
                    result = false;
                    break;
                }
            }

            // Проверки для внутренних узлов
            if (btree->_child != NULL)
            {
                size_t child_count = 0;             // Счётчик потомков
                btree_t * child = btree->_child;    // Потомок

                // Проверка корректности потомков и подсчёт их числа
                for (; child->_right != NULL; child = child->_right)
                {
                    result = result && btIsValid(child);
                    result = result && (child->_cnt < child->_rate - 1);

                    // Проверка верной упорядоченности родительских и дочерних узлов по отношению друг к другу
                    if (strcmp(child->_strs[child->_cnt - 1], btree->_strs[child_count]) > 0)
                    {
                        result = false;
                    }

                    ++child_count;
                }

                // Проверка на число потомков
                if (child_count != cnt +1)
                {
                    result = false;
                }
                // Проверка того, что первый ключ самого правого поддерева больше самого правого ключа дерева
                else if (strcmp(btree->_strs[cnt - 1], child->_strs[0]) > 0)
                {
                    result = false;
                }
            }
        }
        else
        {
                result = false;
        }
    }
    else
    {
        result = false;
    }

    return result;
}

// Инициализация дерева
btree_t * mkBtree(size_t rate)
{
    assert(rate >= 3);      // Минимальная

    btree_t * btree = malloc(sizeof(btree_t));
    btree->_rate    = rate;                                 // Степень
    btree->_cnt     = 0;                                    // Число ключей
    btree->_strs    = calloc(rate, sizeof(char *));         // Ключи
    btree->_child   = NULL;                                 // Потомки
    btree->_right   = NULL;                                 // Правое поддерево

    assert(btIsValid(btree));

    return btree;
}

// Поиск в дереве
bool btHas(btree_t * btree, char * key)
{
    // Проверка на сущ дерева
    if (btree)
    {
        assert(btIsValid(btree));

        // Дополнительные переменные
        btree_t * left = btree->_child;
        char ** strs = btree->_strs;

        // Цикл поиска
        for (size_t i = 0; i < btree->_cnt; ++i)
        {
            // Результат сравнения
            int ratio = strcmp(key, strs[i]);

            if (ratio == 0)         // Успешное нахождение
            {
                return true;
            }
            else if (ratio < 0)     // Переход на следующую итерацию
            {
                if (left)
                {
                    left = left->_right;
                }
                else                // Завершение обхода текущего узла
                {
                    break;
                }
            }

            // Завершение поиска в текущем узле
            else
            {
                break;
            }
        }

        return btHas(left, key);
    }
    else    // Если дерево (поддерево) пусто
    {
        return false;
    }
}

// Добавления ключа в дерево с разбиением (Для внутреннего пользования)
void _btAdd(btstack_t * st, char * key, btree_t * right_tree)
{
    // Проверка корректности указателей
    assert(st);
    assert(key);

    // Получаем узел-цель из стека
    btree_t ** btree_ptr = st->btree_ptr;
    btree_t * btree = *btree_ptr;

    char ** strs = btree->_strs;    // Массив ключей для текущего узла

    // Ищем позицию предполагаемой вставки
    size_t index = 0;
    for (size_t i = 0; i < btree->_cnt; ++i)
    {
        if (strcmp(key, strs[i]) > 0)
        {
            index = i + 1;
        }
        else
        {
            break;
        }
    }

    // Предварительная вставка и сортировка
    //=========================================================================
    char * bag = key;   // Временное расположение для ключа
    ++btree->_cnt;      // Увеличиваем счётчик числа ключей

    // Вставляем ключ
    for (size_t i = index; i < btree->_cnt; ++i)
    {
        char * tmp = strs[i];
        strs[i] = bag;
        bag = tmp;
    }

    // Вставляем узел поддерева (если текущий узел не лист)
    if (right_tree)
    {
        btree_t * child = btree->_child;

        for (size_t i = 0; i < index; ++i)
        {
            child = child->_right;
        }

        // Вставляем поддерево в список
        right_tree->_right = child->_right;
        child->_right = right_tree;
    }

    // Дробление после вставки в заполненный узел
    //=========================================================================
    if (btree->_cnt == btree->_rate - 1)
    {//////////////////////////////////////////////////////////////////////////////////////////////
        // Левая часть после дробления
        btree_t * left_part = btree;                // Изменяем текущий элемент
        left_part->_cnt = btree->_rate / 2;         // Меняем количество элементов

        // Правая часть после дробления
        btree_t * right_part = mkBtree(btree->_rate);
        right_part->_cnt = (btree->_rate - 1) - (btree->_rate / 2);
        memcpy(right_part->_strs, &btree->_strs[btree->_rate / 2 + 1], right_part->_cnt * sizeof(char *));

        // Работа с потомками
        if (btree->_child)
        {
            // Точка разрыва в изначальном (теперь левом узле)
            btree_t * cut_point = btree->_child;
            for(size_t i = 0; i < btree->_rate / 2; ++i)
            {
                cut_point = cut_point->_right;
            }

            // Вставка потомков в правый подузел
            right_part->_child = cut_point->_right;
            cut_point->_right = NULL;
        }

        // Рекурсивно поднимаемся
        if (st && st->parent)
        {
            // Двигаемся дальше по стеку
            st = btstack_pop(st);
            _btAdd(st, btree->_strs[btree->_rate / 2], right_part);
        }
        // Создаём новый корень
        else
        {
            btree_t * new_root = mkBtree(btree->_rate);

            new_root->_cnt = 1;
            new_root->_strs[0] = btree->_strs[btree->_rate / 2];
            new_root->_child = left_part;
            left_part->_right = right_part;

            *btree_ptr = new_root;
        }
    }

    return;
}

// Добавление ключа в дерево
void btAdd(btree_t ** btree_ptr, char * key)
{
    // Проверка корректности указателей
    assert(btree_ptr);  assert(btIsValid(*btree_ptr));
    assert(key);

    btstack_t * st = btstack_push(NULL, btree_ptr);     // Инициализируем стек с помощью корня
    btree_t * root = *btree_ptr;                        // Получаем указатель на корень

    // Дополнительные переменные
    btree_t ** left = NULL;     // Двойной указатель на первого потомка в дереве
    char ** strs = NULL;        // Текущий набор ключей

    // Поиск подходящего листа
    for (btree_t * node = root; node->_child != NULL; node = *left)  // Вертикальный обход узлов
    {
        left = &node->_child;   // Двойной указатель на текущего потомка в дереве
        strs = node->_strs;     // Текущий набор ключей

        for (size_t i = 0; i < node->_cnt; ++i)     // Обход ключей внутри узла
        {
            // Результат сравнения
            int ratio = strcmp(key, strs[i]);

            // Если ключ меньше
            if (ratio < 0)
            {
                break;
            }

            // Двигаем указатель на соответствующую ветку
            left = &(*left)->_right;
        }

        // Заполнение стека
        st = btstack_push(st, left);    // Заполнение стека
    }

    _btAdd(st, key, NULL);              // Выполняем вставку

    assert(btIsValid(*btree_ptr));
    return;
}

// Удавление из дерева
bool _btDel(btstack_t * st, char * key)
{
    // Проверка корректности указателей
    assert(st);
    assert(key);

    // Получаем узел-цель из стека
    btree_t ** btree_ptr = st->btree_ptr;
    btree_t * btree = *btree_ptr;

    char ** strs = btree->_strs;    // Массив ключей для текущего узла

    // Ищем позицию предполагаемой вставки
    size_t index = 0;
    for (size_t i = 0; i < btree->_cnt; ++i)
    {
        if (strcmp(key, strs[i]) == 0)
        {
            index = i;
            break;
        }
    }

    return true;
}

// Удаление из дерева
bool btDel(btree_t ** btree_ptr, char * key)
{
    // Проверка корректности указателей
    assert(btree_ptr);  assert(*btree_ptr);
    assert(key);

    btstack_t * st = btstack_push(NULL, btree_ptr);     // Инициализируем стек с помощью корня
    btree_t * root = *btree_ptr;                        // Получаем указатель на корень

    // Дополнительные переменные
    btree_t ** left = &root->_child;    // Двойной указатель на первого потомка в дереве
    char ** strs = root->_strs;         // Текущий набор ключей

    // Поиск подходящего листа
    for (btree_t * node = root; node->_child != NULL; st = btstack_push(st, left))  // Вертикальный обход узлов
    {
        // Цикл поиска
        for (size_t i = 0; i < node->_cnt; ++i)
        {
            // Результат сравнения
            int ratio = strcmp(key, strs[i]);

            if (ratio == 0)                     // Успешное нахождение
            {
                return _btDel(st, key);
            }
            else if (ratio < 0)  // Переход на следующую итерацию
            {
                if (*left)
                {
                    left = &(*left)->_right;
                }
                else             // Завершение обхода текущего узла
                {
                    break;
                }
            }
        }

        // Меняем узел
        node = *left;
        left = &node->_child;
        strs = root->_strs;
    }

    while ((st = btstack_pop(st)));       // Очищаем стек
    return false;
}

// Тестовые данные
char keys[][50] = {
    "Black",
    "Pink",
    "Blue",
    "White",
    "Red",
    "Green",
    "Yellow",
    "White",
    "Grey",
    "Mango",
    "Orange"
};

// Точка входа
int main()
{
    btree_t * btree = mkBtree(8);
    char a[] = "Pink";

    for (size_t i = 0; i < sizeof(keys) / sizeof (*keys); ++i)
    {
        btAdd(&btree, &keys[i][0]);
    }

    /*
    btree->_cnt = 2;

    btree->_strs[0] = "Black";
    btree->_strs[1] = "Pink";

    btree_t ** child = &btree->_child;

    for (size_t i = 0; i < 3; ++i)
    {
        *child = mkBtree(btree->_rate);

        (*child)->_strs[0] = keys[i + 2];
        (*child)->_cnt = 1;
        child = &(*child)->_child;
    }
    */

    if (btree)
    {
        btView(btree);
    }

    /*
    if (btHas(btree, a))
    {
        printf("%s \"%s\"", "I has ", a);
    }
    else
    {
        printf("%s \"%s\"", "I has not", a);
    }
    */
}
